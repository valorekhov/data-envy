'use strict';

const mqtt = require('async-mqtt');
const fs = require('fs');

class MqttLogger{

    constructor(host, userName, password, caCertPath, rootTopic){
        this.host = host;
        this.userName = userName;
        this.password = password;
        this.caCertPath = caCertPath;
        this.rootTopic = rootTopic || 'envd'
	this.will = undefined;
    }

    async connect(will){
        if (will){
            will.payload = will.payload || '0';  
	    this.will = will;
        }

        this.client = await mqtt.connectAsync(this.host, {
             clientId: 'envd', 
             username: this.userName, 
             password: this.password,
             connectTimeout: 10*1000,
             ca: this.caCertPath && [fs.readFileSync(this.caCertPath)],
             will: will
        });

	console.log('Conntected to', this.host);

        if (will && will.topic){
                console.log('Setting up will on', will.topic);
                await this.client.publish(will.topic, '1', {retain: true});
	}
    }

    async log(message){
	if (!this.client.connected()){
		// If a drop in connection occured, reconnect before posting;
		console.log('Reconnecting the client')
		await this.connect(this.will);
	}
        try {
            return await this.client.publish(this.rootTopic, JSON.stringify(message));
        } catch (e){
            console.log(e.stack);
        }
    }
}

module.exports = { MqttLogger }
